﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LBC
{
    public partial class UCOrder : UserControl
    {
        public UCOrder()
        {
            InitializeComponent();
        }

        private void btnRafraichir_Click(object sender, EventArgs e)
        {
            ListView listview = new ListView();
            lvOrder.View = View.Details;
            lvOrder.Clear();
            
            lvOrder.Columns.Add("Commande numéro");
            this.lvOrder.Columns[0].Width = 150;
            lvOrder.Columns.Add("Date");
            this.lvOrder.Columns[1].Width = 100;
            lvOrder.Columns.Add("Montant de commande");
            this.lvOrder.Columns[2].Width = 290;
            this.lvOrder.Columns[2].TextAlign = HorizontalAlignment.Right;



            SqlConnection thisconnect = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            SqlCommand cmd = new SqlCommand("SelectOrderUser", thisconnect);
            cmd.CommandType = CommandType.StoredProcedure;
            string id = CurrentUser.currentUser.UserId.ToString() + "-%";
            cmd.Parameters.Add("@id", SqlDbType.VarChar,50);
            cmd.Parameters["@id"].Value = id;
            thisconnect.Open();
            SqlDataReader reader = null;
            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    
                    //test de chargement du buffer 
                    //MessageBox.Show($"{reader.GetValue(0).ToString()}  {reader.GetValue(1).ToString()}  {reader.GetValue(2).ToString()} {reader.GetValue(3).ToString()}");

                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = reader.GetValue(0).ToString();
                    item.SubItems.Add(reader.GetValue(1).ToString().Substring(0,10));
                    item.SubItems.Add(reader.GetValue(2).ToString());
                    lvOrder.Items.Add(item);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                thisconnect.Close();
            }
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            OrderDetails orderDetails = new OrderDetails(lvOrder.FocusedItem.SubItems[0].Text);
            orderDetails.ShowDialog();
        }
    }
}
