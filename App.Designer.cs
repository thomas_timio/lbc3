﻿namespace LBC
{
    partial class App
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(App));
            this.panel1 = new System.Windows.Forms.Panel();
            this.littleSidePanel = new System.Windows.Forms.Panel();
            this.btnAccueil = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtHello = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblHome = new System.Windows.Forms.Button();
            this.lblMyOrders = new System.Windows.Forms.Button();
            this.lblBasket = new System.Windows.Forms.Button();
            this.lblLeave = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ucssd1 = new LBC.UCSSD();
            this.ucScreen1 = new LBC.UCScreen();
            this.ucRam1 = new LBC.UCRam();
            this.ucProc1 = new LBC.UCProc();
            this.ucPower1 = new LBC.UCPower();
            this.ucMouse1 = new LBC.UCMouse();
            this.ucMotherboard1 = new LBC.UCMotherboard();
            this.ucKeyboard1 = new LBC.UCKeyboard();
            this.uchdd1 = new LBC.UCHDD();
            this.ucGraph1 = new LBC.UCGraph();
            this.ucBox1 = new LBC.UCBox();
            this.ucBasket1 = new LBC.UCBasket();
            this.ucAccueil1 = new LBC.UCAccueil();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ucOrder1 = new LBC.UCOrder();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.littleSidePanel);
            this.panel1.Controls.Add(this.btnAccueil);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 900);
            this.panel1.TabIndex = 0;
            // 
            // littleSidePanel
            // 
            this.littleSidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(29)))), ((int)(((byte)(0)))));
            this.littleSidePanel.Location = new System.Drawing.Point(0, 0);
            this.littleSidePanel.Name = "littleSidePanel";
            this.littleSidePanel.Size = new System.Drawing.Size(10, 46);
            this.littleSidePanel.TabIndex = 3;
            // 
            // btnAccueil
            // 
            this.btnAccueil.FlatAppearance.BorderSize = 0;
            this.btnAccueil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccueil.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccueil.ForeColor = System.Drawing.Color.White;
            this.btnAccueil.Location = new System.Drawing.Point(0, 0);
            this.btnAccueil.Name = "btnAccueil";
            this.btnAccueil.Size = new System.Drawing.Size(200, 44);
            this.btnAccueil.TabIndex = 29;
            this.btnAccueil.Text = "Accueil";
            this.btnAccueil.UseVisualStyleBackColor = true;
            this.btnAccueil.Click += new System.EventHandler(this.button14_Click);
            // 
            // button12
            // 
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(0, 504);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(200, 44);
            this.button12.TabIndex = 25;
            this.button12.Text = "Claviers";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 52);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Processeurs";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(0, 102);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 44);
            this.button2.TabIndex = 5;
            this.button2.Text = "Cartes Mères";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(0, 152);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(200, 44);
            this.button3.TabIndex = 7;
            this.button3.Text = "Mémoire RAM";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button13
            // 
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(0, 554);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(200, 44);
            this.button13.TabIndex = 27;
            this.button13.Text = "Souris";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(0, 202);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(200, 44);
            this.button4.TabIndex = 9;
            this.button4.Text = "Cartes Graphiques";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(0, 252);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(200, 45);
            this.button6.TabIndex = 13;
            this.button6.Text = "Alimentations";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(0, 303);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(200, 45);
            this.button7.TabIndex = 15;
            this.button7.Text = "SSD";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(0, 354);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(200, 44);
            this.button8.TabIndex = 17;
            this.button8.Text = "HDD";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button11
            // 
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(0, 454);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(200, 44);
            this.button11.TabIndex = 23;
            this.button11.Text = "Écrans";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Location = new System.Drawing.Point(0, 404);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(200, 44);
            this.button10.TabIndex = 21;
            this.button10.Text = "Boitiers";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirstName.Enabled = false;
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.ForeColor = System.Drawing.Color.White;
            this.txtFirstName.Location = new System.Drawing.Point(88, 3);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(350, 23);
            this.txtFirstName.TabIndex = 11;
            // 
            // txtHello
            // 
            this.txtHello.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.txtHello.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtHello.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHello.ForeColor = System.Drawing.Color.White;
            this.txtHello.Location = new System.Drawing.Point(6, 3);
            this.txtHello.MinimumSize = new System.Drawing.Size(0, 30);
            this.txtHello.Name = "txtHello";
            this.txtHello.Size = new System.Drawing.Size(75, 23);
            this.txtHello.TabIndex = 7;
            this.txtHello.Text = "Bonjour,";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(29)))), ((int)(((byte)(0)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(200, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1100, 15);
            this.panel2.TabIndex = 12;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(29)))), ((int)(((byte)(0)))));
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Location = new System.Drawing.Point(200, 15);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(130, 100);
            this.panel3.TabIndex = 13;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 53);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // lblHome
            // 
            this.lblHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHome.Location = new System.Drawing.Point(778, 15);
            this.lblHome.Name = "lblHome";
            this.lblHome.Size = new System.Drawing.Size(150, 40);
            this.lblHome.TabIndex = 14;
            this.lblHome.Text = "Accueil";
            this.lblHome.UseVisualStyleBackColor = true;
            this.lblHome.Click += new System.EventHandler(this.lblHome_Click);
            // 
            // lblMyOrders
            // 
            this.lblMyOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblMyOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMyOrders.Location = new System.Drawing.Point(928, 15);
            this.lblMyOrders.Name = "lblMyOrders";
            this.lblMyOrders.Size = new System.Drawing.Size(150, 40);
            this.lblMyOrders.TabIndex = 15;
            this.lblMyOrders.Text = "Commandes";
            this.lblMyOrders.UseVisualStyleBackColor = true;
            this.lblMyOrders.Click += new System.EventHandler(this.lblMyOrders_Click);
            // 
            // lblBasket
            // 
            this.lblBasket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBasket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBasket.Location = new System.Drawing.Point(1078, 15);
            this.lblBasket.Name = "lblBasket";
            this.lblBasket.Size = new System.Drawing.Size(150, 40);
            this.lblBasket.TabIndex = 16;
            this.lblBasket.Text = "Panier";
            this.lblBasket.UseVisualStyleBackColor = true;
            this.lblBasket.Click += new System.EventHandler(this.lblBasket_Click);
            // 
            // lblLeave
            // 
            this.lblLeave.FlatAppearance.BorderSize = 0;
            this.lblLeave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLeave.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeave.ForeColor = System.Drawing.Color.White;
            this.lblLeave.Image = ((System.Drawing.Image)(resources.GetObject("lblLeave.Image")));
            this.lblLeave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLeave.Location = new System.Drawing.Point(1235, 15);
            this.lblLeave.Margin = new System.Windows.Forms.Padding(4);
            this.lblLeave.Name = "lblLeave";
            this.lblLeave.Size = new System.Drawing.Size(52, 40);
            this.lblLeave.TabIndex = 17;
            this.lblLeave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.lblLeave.UseVisualStyleBackColor = true;
            this.lblLeave.Click += new System.EventHandler(this.lblLeave_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel4.Controls.Add(this.txtHello);
            this.panel4.Controls.Add(this.txtFirstName);
            this.panel4.Location = new System.Drawing.Point(330, 15);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(448, 31);
            this.panel4.TabIndex = 14;
            // 
            // ucssd1
            // 
            this.ucssd1.Location = new System.Drawing.Point(200, 120);
            this.ucssd1.Name = "ucssd1";
            this.ucssd1.Size = new System.Drawing.Size(1100, 840);
            this.ucssd1.TabIndex = 30;
            // 
            // ucScreen1
            // 
            this.ucScreen1.Location = new System.Drawing.Point(200, 120);
            this.ucScreen1.Name = "ucScreen1";
            this.ucScreen1.Size = new System.Drawing.Size(1100, 840);
            this.ucScreen1.TabIndex = 29;
            // 
            // ucRam1
            // 
            this.ucRam1.Location = new System.Drawing.Point(200, 120);
            this.ucRam1.Name = "ucRam1";
            this.ucRam1.Size = new System.Drawing.Size(1100, 840);
            this.ucRam1.TabIndex = 28;
            // 
            // ucProc1
            // 
            this.ucProc1.Location = new System.Drawing.Point(200, 120);
            this.ucProc1.Name = "ucProc1";
            this.ucProc1.Size = new System.Drawing.Size(1100, 840);
            this.ucProc1.TabIndex = 27;
            // 
            // ucPower1
            // 
            this.ucPower1.Location = new System.Drawing.Point(200, 120);
            this.ucPower1.Name = "ucPower1";
            this.ucPower1.Size = new System.Drawing.Size(1100, 840);
            this.ucPower1.TabIndex = 26;
            // 
            // ucMouse1
            // 
            this.ucMouse1.Location = new System.Drawing.Point(200, 120);
            this.ucMouse1.Name = "ucMouse1";
            this.ucMouse1.Size = new System.Drawing.Size(1100, 840);
            this.ucMouse1.TabIndex = 25;
            // 
            // ucMotherboard1
            // 
            this.ucMotherboard1.Location = new System.Drawing.Point(200, 120);
            this.ucMotherboard1.Name = "ucMotherboard1";
            this.ucMotherboard1.Size = new System.Drawing.Size(1100, 840);
            this.ucMotherboard1.TabIndex = 24;
            // 
            // ucKeyboard1
            // 
            this.ucKeyboard1.Location = new System.Drawing.Point(200, 120);
            this.ucKeyboard1.Name = "ucKeyboard1";
            this.ucKeyboard1.Size = new System.Drawing.Size(1100, 840);
            this.ucKeyboard1.TabIndex = 23;
            // 
            // uchdd1
            // 
            this.uchdd1.Location = new System.Drawing.Point(200, 120);
            this.uchdd1.Name = "uchdd1";
            this.uchdd1.Size = new System.Drawing.Size(1100, 840);
            this.uchdd1.TabIndex = 22;
            // 
            // ucGraph1
            // 
            this.ucGraph1.Location = new System.Drawing.Point(200, 120);
            this.ucGraph1.Name = "ucGraph1";
            this.ucGraph1.Size = new System.Drawing.Size(1100, 840);
            this.ucGraph1.TabIndex = 21;
            // 
            // ucBox1
            // 
            this.ucBox1.Location = new System.Drawing.Point(200, 120);
            this.ucBox1.Name = "ucBox1";
            this.ucBox1.Size = new System.Drawing.Size(1100, 840);
            this.ucBox1.TabIndex = 20;
            // 
            // ucBasket1
            // 
            this.ucBasket1.Location = new System.Drawing.Point(200, 120);
            this.ucBasket1.Name = "ucBasket1";
            this.ucBasket1.Size = new System.Drawing.Size(1100, 840);
            this.ucBasket1.TabIndex = 19;
            // 
            // ucAccueil1
            // 
            this.ucAccueil1.BackColor = System.Drawing.SystemColors.Control;
            this.ucAccueil1.Location = new System.Drawing.Point(200, 120);
            this.ucAccueil1.Name = "ucAccueil1";
            this.ucAccueil1.Size = new System.Drawing.Size(1100, 840);
            this.ucAccueil1.TabIndex = 18;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel5.Location = new System.Drawing.Point(200, 114);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(130, 1);
            this.panel5.TabIndex = 31;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel6.Location = new System.Drawing.Point(129, -1);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1, 100);
            this.panel6.TabIndex = 32;
            // 
            // ucOrder1
            // 
            this.ucOrder1.Location = new System.Drawing.Point(200, 120);
            this.ucOrder1.Name = "ucOrder1";
            this.ucOrder1.Size = new System.Drawing.Size(1100, 780);
            this.ucOrder1.TabIndex = 32;
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1300, 900);
            this.Controls.Add(this.ucOrder1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.ucssd1);
            this.Controls.Add(this.ucScreen1);
            this.Controls.Add(this.ucRam1);
            this.Controls.Add(this.ucProc1);
            this.Controls.Add(this.ucPower1);
            this.Controls.Add(this.ucMouse1);
            this.Controls.Add(this.ucMotherboard1);
            this.Controls.Add(this.ucKeyboard1);
            this.Controls.Add(this.uchdd1);
            this.Controls.Add(this.ucGraph1);
            this.Controls.Add(this.ucBox1);
            this.Controls.Add(this.ucBasket1);
            this.Controls.Add(this.ucAccueil1);
            this.Controls.Add(this.lblLeave);
            this.Controls.Add(this.lblBasket);
            this.Controls.Add(this.lblMyOrders);
            this.Controls.Add(this.lblHome);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "App";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LBC";
            this.Load += new System.EventHandler(this.App_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button btnAccueil;
        private System.Windows.Forms.Panel littleSidePanel;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtHello;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button lblHome;
        private System.Windows.Forms.Button lblMyOrders;
        private System.Windows.Forms.Button lblBasket;
        private System.Windows.Forms.Button lblLeave;
        private UCAccueil ucAccueil1;
        private UCBasket ucBasket1;
        private UCBox ucBox1;
        private UCGraph ucGraph1;
        private UCHDD uchdd1;
        private UCKeyboard ucKeyboard1;
        private UCMotherboard ucMotherboard1;
        private UCMouse ucMouse1;
        private UCPower ucPower1;
        private UCProc ucProc1;
        private UCRam ucRam1;
        private UCScreen ucScreen1;
        private UCSSD ucssd1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private UCOrder ucOrder1;
    }
}

