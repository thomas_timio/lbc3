﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LBC
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        //Clique sur bouton "Se connecter"
        private void btnSignin_Click(object sender, EventArgs e)
        {
            Signin();

        }

        //Clique sur bouton "S'inscrire"
        private void btnSignup_Click(object sender, EventArgs e)
        {
            Signup open = new Signup(txtUsername.Text, txtPassword.Text);
            open.ShowDialog();
        }

        private void btnApp_Click(object sender, EventArgs e)
        {
            App open = new App();
            open.ShowDialog();
            this.Close();
        }

        private void Signin()
        {
            string NewElmt;
            //Connexion à la base de données
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            //Requête SignIn via la procédure stockée
            SqlCommand myCommand = new SqlCommand("SignIn", thisConnection);
            myCommand.CommandType = CommandType.StoredProcedure;
            //variables
            myCommand.Parameters.Add("@username", SqlDbType.VarChar, 50);
            myCommand.Parameters["@username"].Value = txtUsername.Text;
            try
            {
                NewElmt = (string)myCommand.ExecuteScalar();
                if (NewElmt == txtPassword.Text)
                {
                    //Création des variables nécessaire currentUser
                    int id = 0;
                    string lastname = " ";
                    string firstname = " ";
                    string username = " ";
                    string password = " ";
                    string email = " ";
                    string adress = " ";
                    string phone = " ";


                    //Requête via select
                    SqlCommand cmd = new SqlCommand("SELECT * FROM users WHERE username = @username", thisConnection);
                    cmd.Parameters.Add("@username", SqlDbType.VarChar, 50);
                    cmd.Parameters["@username"].Value = txtUsername.Text;
                    //Add Parameters to Command Parameters collection

                    SqlDataReader DataRead;
                    DataRead = cmd.ExecuteReader();
                    //Récupération des données utilisateur
                    while (DataRead.Read())
                    {
                        id = (int)DataRead["id"];
                        lastname = DataRead["lastname"].ToString();
                        firstname = DataRead["firstname"].ToString();
                        username = DataRead["username"].ToString();
                        password = DataRead["password"].ToString();
                        email = DataRead["email"].ToString();
                        adress = DataRead["adress"].ToString();
                        phone = DataRead["phone"].ToString();
                    }

                    CurrentUser.currentUser.UserId = id;
                    CurrentUser.currentUser.UserLastname = lastname;
                    CurrentUser.currentUser.UserFirstname = firstname;
                    CurrentUser.currentUser.UserUsername = username;
                    CurrentUser.currentUser.UserPassword = password;
                    CurrentUser.currentUser.UserEmail = email;
                    CurrentUser.currentUser.UserPhone = phone;
                    CurrentUser.currentUser.UserAdress = adress;
                    //Fin de récupération des données utilisateur

                    //Ouverture de l'app, fermeture du login
                    App open = new App();
                    open.ShowDialog();
                    Login close = new Login();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Nom d'utilisateur ou mot de passe incorrect");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "Erreur!");
                return;
            }
            thisConnection.Close();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)

            {
                Signin();
            }
        }

        private void linkPassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ForgottenPassword open = new ForgottenPassword();
            open.ShowDialog();
        }

        private void labLeave_LinkClicked(object sender, EventArgs e)
        {
            var quitter = MessageBox.Show("Voulez vous quitter ?", "Quitter", MessageBoxButtons.YesNo);
            if (quitter == DialogResult.Yes)
            {
                this.Close();
            }

        }
    }
}
