﻿namespace LBC
{
    partial class UCSSD
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCSSD));
            this.btnProduct10 = new System.Windows.Forms.Button();
            this.btnProduct9 = new System.Windows.Forms.Button();
            this.btnProduct8 = new System.Windows.Forms.Button();
            this.btnProduct7 = new System.Windows.Forms.Button();
            this.btnProduct6 = new System.Windows.Forms.Button();
            this.btnProduct5 = new System.Windows.Forms.Button();
            this.btnProduct4 = new System.Windows.Forms.Button();
            this.btnProduct3 = new System.Windows.Forms.Button();
            this.btnProduct2 = new System.Windows.Forms.Button();
            this.btnProduct1 = new System.Windows.Forms.Button();
            this.qty6 = new System.Windows.Forms.NumericUpDown();
            this.button11 = new System.Windows.Forms.Button();
            this.qty10 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice10 = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.button13 = new System.Windows.Forms.Button();
            this.qty9 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice9 = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.button15 = new System.Windows.Forms.Button();
            this.qty8 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice8 = new System.Windows.Forms.Button();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.button17 = new System.Windows.Forms.Button();
            this.qty7 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice7 = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.button19 = new System.Windows.Forms.Button();
            this.btnPrice6 = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.button9 = new System.Windows.Forms.Button();
            this.qty5 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice5 = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.button7 = new System.Windows.Forms.Button();
            this.qty4 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice4 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.qty3 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice3 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.qty2 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice2 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.qty1 = new System.Windows.Forms.NumericUpDown();
            this.btnPrice1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labTitre = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.qty6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnProduct10
            // 
            this.btnProduct10.AutoEllipsis = true;
            this.btnProduct10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct10.FlatAppearance.BorderSize = 0;
            this.btnProduct10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct10.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct10.Location = new System.Drawing.Point(905, 435);
            this.btnProduct10.Name = "btnProduct10";
            this.btnProduct10.Size = new System.Drawing.Size(190, 32);
            this.btnProduct10.TabIndex = 120;
            this.btnProduct10.UseVisualStyleBackColor = false;
            this.btnProduct10.Click += new System.EventHandler(this.btnProduct10_Click);
            // 
            // btnProduct9
            // 
            this.btnProduct9.AutoEllipsis = true;
            this.btnProduct9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct9.FlatAppearance.BorderSize = 0;
            this.btnProduct9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct9.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct9.Location = new System.Drawing.Point(680, 435);
            this.btnProduct9.Name = "btnProduct9";
            this.btnProduct9.Size = new System.Drawing.Size(190, 32);
            this.btnProduct9.TabIndex = 119;
            this.btnProduct9.UseVisualStyleBackColor = false;
            this.btnProduct9.Click += new System.EventHandler(this.btnProduct9_Click);
            // 
            // btnProduct8
            // 
            this.btnProduct8.AutoEllipsis = true;
            this.btnProduct8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct8.FlatAppearance.BorderSize = 0;
            this.btnProduct8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct8.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct8.Location = new System.Drawing.Point(455, 435);
            this.btnProduct8.Name = "btnProduct8";
            this.btnProduct8.Size = new System.Drawing.Size(190, 32);
            this.btnProduct8.TabIndex = 118;
            this.btnProduct8.UseVisualStyleBackColor = false;
            this.btnProduct8.Click += new System.EventHandler(this.btnProduct8_Click);
            // 
            // btnProduct7
            // 
            this.btnProduct7.AutoEllipsis = true;
            this.btnProduct7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct7.FlatAppearance.BorderSize = 0;
            this.btnProduct7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct7.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct7.Location = new System.Drawing.Point(230, 435);
            this.btnProduct7.Name = "btnProduct7";
            this.btnProduct7.Size = new System.Drawing.Size(190, 32);
            this.btnProduct7.TabIndex = 117;
            this.btnProduct7.UseVisualStyleBackColor = false;
            this.btnProduct7.Click += new System.EventHandler(this.btnProduct7_Click);
            // 
            // btnProduct6
            // 
            this.btnProduct6.AutoEllipsis = true;
            this.btnProduct6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct6.FlatAppearance.BorderSize = 0;
            this.btnProduct6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct6.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct6.Location = new System.Drawing.Point(5, 435);
            this.btnProduct6.Name = "btnProduct6";
            this.btnProduct6.Size = new System.Drawing.Size(190, 32);
            this.btnProduct6.TabIndex = 116;
            this.btnProduct6.UseVisualStyleBackColor = false;
            this.btnProduct6.Click += new System.EventHandler(this.btnProduct6_Click);
            // 
            // btnProduct5
            // 
            this.btnProduct5.AutoEllipsis = true;
            this.btnProduct5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct5.FlatAppearance.BorderSize = 0;
            this.btnProduct5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct5.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct5.Location = new System.Drawing.Point(907, 40);
            this.btnProduct5.Name = "btnProduct5";
            this.btnProduct5.Size = new System.Drawing.Size(190, 32);
            this.btnProduct5.TabIndex = 115;
            this.btnProduct5.UseVisualStyleBackColor = false;
            this.btnProduct5.Click += new System.EventHandler(this.btnProduct5_Click);
            // 
            // btnProduct4
            // 
            this.btnProduct4.AutoEllipsis = true;
            this.btnProduct4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct4.FlatAppearance.BorderSize = 0;
            this.btnProduct4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct4.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct4.Location = new System.Drawing.Point(682, 40);
            this.btnProduct4.Name = "btnProduct4";
            this.btnProduct4.Size = new System.Drawing.Size(190, 32);
            this.btnProduct4.TabIndex = 114;
            this.btnProduct4.UseVisualStyleBackColor = false;
            this.btnProduct4.Click += new System.EventHandler(this.btnProduct4_Click);
            // 
            // btnProduct3
            // 
            this.btnProduct3.AutoEllipsis = true;
            this.btnProduct3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct3.FlatAppearance.BorderSize = 0;
            this.btnProduct3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct3.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct3.Location = new System.Drawing.Point(457, 40);
            this.btnProduct3.Name = "btnProduct3";
            this.btnProduct3.Size = new System.Drawing.Size(190, 32);
            this.btnProduct3.TabIndex = 113;
            this.btnProduct3.UseVisualStyleBackColor = false;
            this.btnProduct3.Click += new System.EventHandler(this.btnProduct3_Click);
            // 
            // btnProduct2
            // 
            this.btnProduct2.AutoEllipsis = true;
            this.btnProduct2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct2.FlatAppearance.BorderSize = 0;
            this.btnProduct2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct2.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct2.Location = new System.Drawing.Point(232, 40);
            this.btnProduct2.Name = "btnProduct2";
            this.btnProduct2.Size = new System.Drawing.Size(190, 32);
            this.btnProduct2.TabIndex = 112;
            this.btnProduct2.UseVisualStyleBackColor = false;
            this.btnProduct2.Click += new System.EventHandler(this.btnProduct2_Click);
            // 
            // btnProduct1
            // 
            this.btnProduct1.AutoEllipsis = true;
            this.btnProduct1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProduct1.FlatAppearance.BorderSize = 0;
            this.btnProduct1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct1.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduct1.Location = new System.Drawing.Point(7, 40);
            this.btnProduct1.Name = "btnProduct1";
            this.btnProduct1.Size = new System.Drawing.Size(190, 32);
            this.btnProduct1.TabIndex = 111;
            this.btnProduct1.UseVisualStyleBackColor = false;
            this.btnProduct1.Click += new System.EventHandler(this.btnProduct1_Click);
            // 
            // qty6
            // 
            this.qty6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty6.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty6.Location = new System.Drawing.Point(7, 690);
            this.qty6.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty6.Name = "qty6";
            this.qty6.Size = new System.Drawing.Size(45, 28);
            this.qty6.TabIndex = 110;
            this.qty6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.PaleGreen;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(948, 689);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(147, 28);
            this.button11.TabIndex = 109;
            this.button11.Text = "Ajouter au panier";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // qty10
            // 
            this.qty10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty10.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty10.Location = new System.Drawing.Point(905, 689);
            this.qty10.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty10.Name = "qty10";
            this.qty10.Size = new System.Drawing.Size(45, 28);
            this.qty10.TabIndex = 108;
            this.qty10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice10
            // 
            this.btnPrice10.BackColor = System.Drawing.Color.Red;
            this.btnPrice10.Enabled = false;
            this.btnPrice10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice10.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice10.Location = new System.Drawing.Point(905, 639);
            this.btnPrice10.Name = "btnPrice10";
            this.btnPrice10.Size = new System.Drawing.Size(190, 39);
            this.btnPrice10.TabIndex = 107;
            this.btnPrice10.Text = " ";
            this.btnPrice10.UseVisualStyleBackColor = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(905, 458);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(190, 190);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 106;
            this.pictureBox6.TabStop = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.PaleGreen;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(723, 691);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(147, 28);
            this.button13.TabIndex = 105;
            this.button13.Text = "Ajouter au panier";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // qty9
            // 
            this.qty9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty9.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty9.Location = new System.Drawing.Point(680, 691);
            this.qty9.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty9.Name = "qty9";
            this.qty9.Size = new System.Drawing.Size(45, 28);
            this.qty9.TabIndex = 104;
            this.qty9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice9
            // 
            this.btnPrice9.BackColor = System.Drawing.Color.Red;
            this.btnPrice9.Enabled = false;
            this.btnPrice9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice9.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice9.Location = new System.Drawing.Point(680, 641);
            this.btnPrice9.Name = "btnPrice9";
            this.btnPrice9.Size = new System.Drawing.Size(190, 39);
            this.btnPrice9.TabIndex = 103;
            this.btnPrice9.Text = " ";
            this.btnPrice9.UseVisualStyleBackColor = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(680, 458);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(190, 190);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 102;
            this.pictureBox7.TabStop = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.PaleGreen;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(498, 691);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(147, 28);
            this.button15.TabIndex = 101;
            this.button15.Text = "Ajouter au panier";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // qty8
            // 
            this.qty8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty8.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty8.Location = new System.Drawing.Point(455, 691);
            this.qty8.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty8.Name = "qty8";
            this.qty8.Size = new System.Drawing.Size(45, 28);
            this.qty8.TabIndex = 100;
            this.qty8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice8
            // 
            this.btnPrice8.BackColor = System.Drawing.Color.Red;
            this.btnPrice8.Enabled = false;
            this.btnPrice8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice8.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice8.Location = new System.Drawing.Point(455, 641);
            this.btnPrice8.Name = "btnPrice8";
            this.btnPrice8.Size = new System.Drawing.Size(190, 39);
            this.btnPrice8.TabIndex = 99;
            this.btnPrice8.Text = " ";
            this.btnPrice8.UseVisualStyleBackColor = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(455, 458);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(190, 190);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 98;
            this.pictureBox8.TabStop = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.PaleGreen;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(273, 691);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(147, 28);
            this.button17.TabIndex = 97;
            this.button17.Text = "Ajouter au panier";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // qty7
            // 
            this.qty7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty7.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty7.Location = new System.Drawing.Point(230, 691);
            this.qty7.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty7.Name = "qty7";
            this.qty7.Size = new System.Drawing.Size(45, 28);
            this.qty7.TabIndex = 96;
            this.qty7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice7
            // 
            this.btnPrice7.BackColor = System.Drawing.Color.Red;
            this.btnPrice7.Enabled = false;
            this.btnPrice7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice7.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice7.Location = new System.Drawing.Point(230, 641);
            this.btnPrice7.Name = "btnPrice7";
            this.btnPrice7.Size = new System.Drawing.Size(190, 39);
            this.btnPrice7.TabIndex = 95;
            this.btnPrice7.Text = " ";
            this.btnPrice7.UseVisualStyleBackColor = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(230, 458);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(190, 190);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 94;
            this.pictureBox9.TabStop = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.PaleGreen;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Location = new System.Drawing.Point(48, 691);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(147, 28);
            this.button19.TabIndex = 93;
            this.button19.Text = "Ajouter au panier";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // btnPrice6
            // 
            this.btnPrice6.BackColor = System.Drawing.Color.Red;
            this.btnPrice6.Enabled = false;
            this.btnPrice6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice6.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice6.Location = new System.Drawing.Point(5, 641);
            this.btnPrice6.Name = "btnPrice6";
            this.btnPrice6.Size = new System.Drawing.Size(190, 39);
            this.btnPrice6.TabIndex = 92;
            this.btnPrice6.Text = " ";
            this.btnPrice6.UseVisualStyleBackColor = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(5, 458);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(190, 190);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 91;
            this.pictureBox10.TabStop = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.PaleGreen;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(950, 296);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(147, 28);
            this.button9.TabIndex = 90;
            this.button9.Text = "Ajouter au panier";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // qty5
            // 
            this.qty5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty5.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty5.Location = new System.Drawing.Point(907, 296);
            this.qty5.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty5.Name = "qty5";
            this.qty5.Size = new System.Drawing.Size(45, 28);
            this.qty5.TabIndex = 89;
            this.qty5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice5
            // 
            this.btnPrice5.BackColor = System.Drawing.Color.Red;
            this.btnPrice5.Enabled = false;
            this.btnPrice5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice5.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice5.Location = new System.Drawing.Point(907, 246);
            this.btnPrice5.Name = "btnPrice5";
            this.btnPrice5.Size = new System.Drawing.Size(190, 39);
            this.btnPrice5.TabIndex = 88;
            this.btnPrice5.Text = " ";
            this.btnPrice5.UseVisualStyleBackColor = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(907, 65);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(190, 190);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 87;
            this.pictureBox5.TabStop = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.PaleGreen;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(725, 298);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(147, 28);
            this.button7.TabIndex = 86;
            this.button7.Text = "Ajouter au panier";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // qty4
            // 
            this.qty4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty4.Location = new System.Drawing.Point(682, 298);
            this.qty4.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty4.Name = "qty4";
            this.qty4.Size = new System.Drawing.Size(45, 28);
            this.qty4.TabIndex = 85;
            this.qty4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice4
            // 
            this.btnPrice4.BackColor = System.Drawing.Color.Red;
            this.btnPrice4.Enabled = false;
            this.btnPrice4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice4.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice4.Location = new System.Drawing.Point(682, 248);
            this.btnPrice4.Name = "btnPrice4";
            this.btnPrice4.Size = new System.Drawing.Size(190, 39);
            this.btnPrice4.TabIndex = 84;
            this.btnPrice4.Text = " ";
            this.btnPrice4.UseVisualStyleBackColor = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(682, 65);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(190, 190);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 83;
            this.pictureBox4.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.PaleGreen;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(500, 298);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(147, 28);
            this.button5.TabIndex = 82;
            this.button5.Text = "Ajouter au panier";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // qty3
            // 
            this.qty3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty3.Location = new System.Drawing.Point(457, 298);
            this.qty3.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty3.Name = "qty3";
            this.qty3.Size = new System.Drawing.Size(45, 28);
            this.qty3.TabIndex = 81;
            this.qty3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice3
            // 
            this.btnPrice3.BackColor = System.Drawing.Color.Red;
            this.btnPrice3.Enabled = false;
            this.btnPrice3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice3.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice3.Location = new System.Drawing.Point(457, 248);
            this.btnPrice3.Name = "btnPrice3";
            this.btnPrice3.Size = new System.Drawing.Size(190, 39);
            this.btnPrice3.TabIndex = 80;
            this.btnPrice3.Text = " ";
            this.btnPrice3.UseVisualStyleBackColor = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(457, 65);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(190, 190);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 79;
            this.pictureBox3.TabStop = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.PaleGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(275, 298);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(147, 28);
            this.button3.TabIndex = 78;
            this.button3.Text = "Ajouter au panier";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // qty2
            // 
            this.qty2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty2.Location = new System.Drawing.Point(232, 298);
            this.qty2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty2.Name = "qty2";
            this.qty2.Size = new System.Drawing.Size(45, 28);
            this.qty2.TabIndex = 77;
            this.qty2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice2
            // 
            this.btnPrice2.BackColor = System.Drawing.Color.Red;
            this.btnPrice2.Enabled = false;
            this.btnPrice2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice2.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice2.Location = new System.Drawing.Point(232, 248);
            this.btnPrice2.Name = "btnPrice2";
            this.btnPrice2.Size = new System.Drawing.Size(190, 39);
            this.btnPrice2.TabIndex = 76;
            this.btnPrice2.Text = " ";
            this.btnPrice2.UseVisualStyleBackColor = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(232, 65);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(190, 190);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 75;
            this.pictureBox2.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.PaleGreen;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(50, 298);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 28);
            this.button2.TabIndex = 74;
            this.button2.Text = "Ajouter au panier";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // qty1
            // 
            this.qty1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qty1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty1.Location = new System.Drawing.Point(7, 298);
            this.qty1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.qty1.Name = "qty1";
            this.qty1.Size = new System.Drawing.Size(45, 28);
            this.qty1.TabIndex = 73;
            this.qty1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrice1
            // 
            this.btnPrice1.BackColor = System.Drawing.Color.Red;
            this.btnPrice1.Enabled = false;
            this.btnPrice1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrice1.Font = new System.Drawing.Font("Impact", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrice1.Location = new System.Drawing.Point(7, 248);
            this.btnPrice1.Name = "btnPrice1";
            this.btnPrice1.Size = new System.Drawing.Size(190, 39);
            this.btnPrice1.TabIndex = 72;
            this.btnPrice1.Text = " ";
            this.btnPrice1.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(7, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(190, 190);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 71;
            this.pictureBox1.TabStop = false;
            // 
            // labTitre
            // 
            this.labTitre.AutoSize = true;
            this.labTitre.Dock = System.Windows.Forms.DockStyle.Top;
            this.labTitre.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTitre.Location = new System.Drawing.Point(0, 0);
            this.labTitre.Name = "labTitre";
            this.labTitre.Size = new System.Drawing.Size(67, 39);
            this.labTitre.TabIndex = 70;
            this.labTitre.Text = "SSD";
            // 
            // UCSSD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnProduct10);
            this.Controls.Add(this.btnProduct9);
            this.Controls.Add(this.btnProduct8);
            this.Controls.Add(this.btnProduct7);
            this.Controls.Add(this.btnProduct6);
            this.Controls.Add(this.btnProduct5);
            this.Controls.Add(this.btnProduct4);
            this.Controls.Add(this.btnProduct3);
            this.Controls.Add(this.btnProduct2);
            this.Controls.Add(this.btnProduct1);
            this.Controls.Add(this.qty6);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.qty10);
            this.Controls.Add(this.btnPrice10);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.qty9);
            this.Controls.Add(this.btnPrice9);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.qty8);
            this.Controls.Add(this.btnPrice8);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.qty7);
            this.Controls.Add(this.btnPrice7);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.btnPrice6);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.qty5);
            this.Controls.Add(this.btnPrice5);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.qty4);
            this.Controls.Add(this.btnPrice4);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.qty3);
            this.Controls.Add(this.btnPrice3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.qty2);
            this.Controls.Add(this.btnPrice2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.qty1);
            this.Controls.Add(this.btnPrice1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labTitre);
            this.Name = "UCSSD";
            this.Size = new System.Drawing.Size(1100, 780);
            this.Load += new System.EventHandler(this.UCSSD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.qty6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnProduct10;
        private System.Windows.Forms.Button btnProduct9;
        private System.Windows.Forms.Button btnProduct8;
        private System.Windows.Forms.Button btnProduct7;
        private System.Windows.Forms.Button btnProduct6;
        private System.Windows.Forms.Button btnProduct5;
        private System.Windows.Forms.Button btnProduct4;
        private System.Windows.Forms.Button btnProduct3;
        private System.Windows.Forms.Button btnProduct2;
        private System.Windows.Forms.Button btnProduct1;
        private System.Windows.Forms.NumericUpDown qty6;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.NumericUpDown qty10;
        private System.Windows.Forms.Button btnPrice10;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.NumericUpDown qty9;
        private System.Windows.Forms.Button btnPrice9;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.NumericUpDown qty8;
        private System.Windows.Forms.Button btnPrice8;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.NumericUpDown qty7;
        private System.Windows.Forms.Button btnPrice7;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button btnPrice6;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.NumericUpDown qty5;
        private System.Windows.Forms.Button btnPrice5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.NumericUpDown qty4;
        private System.Windows.Forms.Button btnPrice4;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.NumericUpDown qty3;
        private System.Windows.Forms.Button btnPrice3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.NumericUpDown qty2;
        private System.Windows.Forms.Button btnPrice2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown qty1;
        private System.Windows.Forms.Button btnPrice1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labTitre;
    }
}
