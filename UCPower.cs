﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LBC
{
    public partial class UCPower : UserControl
    {
        public UCPower()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.AL0001;
            pictureBox2.Image = Properties.Resources.AL0002;
            pictureBox3.Image = Properties.Resources.AL0003;
            pictureBox4.Image = Properties.Resources.AL0004;
            pictureBox5.Image = Properties.Resources.AL0005;
            pictureBox9.Image = Properties.Resources.AL0007;
            pictureBox10.Image = Properties.Resources.AL0006;
        }

        private void UCPower_Load(object sender, EventArgs e)
        {
            //Chargement des produits enregistrés dans la base de données

                //Connexion à la base de données
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                //commande
                SqlCommand cmd = new SqlCommand("SELECT produit,prix,marque FROM alim", thisConnection);

                //Déclaration des listes
                List<string> productName = new List<string>();
                List<double> price = new List<double>();

                SqlDataReader DataRead;
                DataRead = cmd.ExecuteReader();
                //Récupération des données utilisateur
                while (DataRead.Read())
                {
                    productName.Add(DataRead["marque"].ToString() + " " + DataRead["produit"].ToString());
                    price.Add(Double.Parse(DataRead["prix"].ToString()));
                }

                foreach (string name in productName)
                {
                    btnProduct1.Text = productName[0].ToString();
                    btnProduct2.Text = productName[1].ToString();
                    btnProduct3.Text = productName[2].ToString();
                    btnProduct4.Text = productName[3].ToString();
                    btnProduct5.Text = productName[4].ToString();
                    btnProduct6.Text = productName[5].ToString();
                    btnProduct7.Text = productName[6].ToString();

                }

                foreach (double euros in price)
                {
                    btnPrice1.Text = price[0].ToString() + " €";
                    btnPrice2.Text = price[1].ToString() + " €";
                    btnPrice3.Text = price[2].ToString() + " €";
                    btnPrice4.Text = price[3].ToString() + " €";
                    btnPrice5.Text = price[4].ToString() + " €";
                    btnPrice6.Text = price[5].ToString() + " €";
                    btnPrice7.Text = price[6].ToString() + " €";

                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (qty1.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "AL0001";
                myCommand.Parameters["@productName"].Value = btnProduct1.Text;
                myCommand.Parameters["@quantity"].Value = qty1.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice1.Text.Substring(0, btnPrice1.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (qty2.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "AL0002";
                myCommand.Parameters["@productName"].Value = btnProduct2.Text;
                myCommand.Parameters["@quantity"].Value = qty2.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice2.Text.Substring(0, btnPrice2.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (qty3.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "AL0003";
                myCommand.Parameters["@productName"].Value = btnProduct3.Text;
                myCommand.Parameters["@quantity"].Value = qty3.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice3.Text.Substring(0, btnPrice3.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (qty4.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "AL0004";
                myCommand.Parameters["@productName"].Value = btnProduct4.Text;
                myCommand.Parameters["@quantity"].Value = qty4.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice4.Text.Substring(0, btnPrice4.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (qty5.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "AL0005";
                myCommand.Parameters["@productName"].Value = btnProduct5.Text;
                myCommand.Parameters["@quantity"].Value = qty5.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice5.Text.Substring(0, btnPrice5.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (qty6.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "AL0006";
                myCommand.Parameters["@productName"].Value = btnProduct6.Text;
                myCommand.Parameters["@quantity"].Value = qty6.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice6.Text.Substring(0, btnPrice6.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (qty7.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "AL0007";
                myCommand.Parameters["@productName"].Value = btnProduct7.Text;
                myCommand.Parameters["@quantity"].Value = qty7.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice7.Text.Substring(0, btnPrice7.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void btnProduct1_Click(object sender, EventArgs e)
        {
            ProductDetail productDetail = new ProductDetail("AL0001", btnProduct1.Text);
            productDetail.ShowDialog();
        }

        private void btnProduct2_Click(object sender, EventArgs e)
        {
            ProductDetail productDetail = new ProductDetail("AL0002", btnProduct2.Text);
            productDetail.ShowDialog();
        }

        private void btnProduct3_Click(object sender, EventArgs e)
        {
            ProductDetail productDetail = new ProductDetail("AL0003", btnProduct3.Text);
            productDetail.ShowDialog();
        }

        private void btnProduct4_Click(object sender, EventArgs e)
        {
            ProductDetail productDetail = new ProductDetail("AL0004", btnProduct4.Text);
            productDetail.ShowDialog();
        }

        private void btnProduct5_Click(object sender, EventArgs e)
        {
            ProductDetail productDetail = new ProductDetail("AL0005", btnProduct5.Text);
            productDetail.ShowDialog();
        }

        private void btnProduct6_Click(object sender, EventArgs e)
        {
            ProductDetail productDetail = new ProductDetail("AL0006", btnProduct6.Text);
            productDetail.ShowDialog();
        }

        private void btnProduct7_Click(object sender, EventArgs e)
        {
            ProductDetail productDetail = new ProductDetail("AL0007", btnProduct7.Text);
            productDetail.ShowDialog();
        }
    }
}
