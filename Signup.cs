﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace LBC
{
    public partial class Signup : Form
    {
        VerificationTexte verificationTexte = new VerificationTexte();

        public Signup()
        {
            InitializeComponent();
        }

        public Signup(string username, string password)
        {
            InitializeComponent();
            txtUsername.Text = username;
            txtPassword.Text = password;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSignup_Click(object sender, EventArgs e)
        {
            InsertClient();
        }

        private void InsertClient()
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertClient", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@lastname", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@firstname", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@username", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@password", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@email", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@adress", SqlDbType.VarChar, 120);
                myCommand.Parameters.Add("@phone", SqlDbType.VarChar, 10);

                // Affectation des valeurs
                myCommand.Parameters["@lastname"].Value = txtLastname.Text;
                myCommand.Parameters["@firstname"].Value = txtFirstname.Text;
                myCommand.Parameters["@username"].Value = txtUsername.Text;
                myCommand.Parameters["@password"].Value = txtPassword.Text;
                myCommand.Parameters["@email"].Value = txtEmail.Text;
                myCommand.Parameters["@adress"].Value = txtAdress.Text;
                myCommand.Parameters["@phone"].Value = txtPhone.Text;
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Vous avez déjà un compte!");
                    }
                    else
                    {
                        var result = MessageBox.Show("Compte créé avec succès!", "inscription");
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
        }
         

        private void btnVisible_Click(object sender, EventArgs e)
        {
            if (txtPassword.UseSystemPasswordChar == true)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }

        private void labLeave_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Signup_Paint(object sender, PaintEventArgs e)
        {
            //Pen p = new Pen(Color.Red);
            //Graphics g = e.Graphics;
            //g.DrawRectangle(p, new Rectangle(txtLastname.Location.X -1 , txtLastname.Location.Y  -1, txtLastname.Width +1 , txtLastname.Height +1 ));
            //g.DrawRectangle(p, new Rectangle(txtFirstname.Location.X -1 , txtFirstname.Location.Y  -1, txtFirstname.Width  +1, txtFirstname.Height +1 ));
            //g.DrawRectangle(p, new Rectangle(txtUsername.Location.X -1 , txtUsername.Location.Y  -1, txtUsername.Width +1 , txtUsername.Height +1 ));
            //g.DrawRectangle(p, new Rectangle(txtPassword.Location.X -1 , txtPassword.Location.Y  -1, txtPassword.Width  +1, txtPassword.Height +1 ));
            //g.DrawRectangle(p, new Rectangle(txtEmail.Location.X -1 , txtEmail.Location.Y -1 , txtEmail.Width  +1, txtEmail.Height +1 ));
            //g.DrawRectangle(p, new Rectangle(txtAdress.Location.X -1 , txtAdress.Location.Y -1 , txtAdress.Width +1 , txtAdress.Height +1 ));
            //g.DrawRectangle(p, new Rectangle(txtPhone.Location.X -1 , txtPhone.Location.Y -1 , txtPhone.Width +1 , txtPhone.Height +1 ));
        }
        private void txtLastname_Leave(object sender, EventArgs e)
        {
            txtLastname.Text = VerificationTexte.ToUpperCase(txtLastname.Text);
        }

        private void txtFirstname_Leave(object sender, EventArgs e)
        {
            txtFirstname.Text = VerificationTexte.FirstUpperCase(txtFirstname.Text);
        }


        private void txtLastname_Validating(object sender, CancelEventArgs e)
        {
            
            if(!VerificationTexte.isValidNom(txtLastname.Text) || string.IsNullOrEmpty(txtLastname.Text)) 
            {
                e.Cancel = true;
                txtLastname.Focus();
                errorProvider1.SetError(txtLastname, "Veuillez n'entrer que des lettres");
            }
            else
            {
                e.Cancel = false;
                errorProvider1.SetError(txtLastname, null);
            }
        }

        private void txtFirstname_Validating(object sender, CancelEventArgs e)
        {
            if (!VerificationTexte.isValidNom(txtFirstname.Text) || string.IsNullOrEmpty(txtFirstname.Text))
            {
                e.Cancel = true;
                txtFirstname.Focus();
                errorProvider1.SetError(txtFirstname, "Veuillez n'entrer que des lettres");
            }
            else
            {
                e.Cancel = false;
                errorProvider1.SetError(txtFirstname, null);
            }
        }

        private void txtUsername_Validating(object sender, CancelEventArgs e)
        {
            if (!VerificationTexte.isValidUsername(txtUsername.Text) || string.IsNullOrEmpty(txtUsername.Text))
            {
                e.Cancel = true;
                txtUsername.Focus();
                errorProvider1.SetError(txtUsername, "Veuillez n'entrer que des lettres et des chiffres");
            }
            else
            {
                e.Cancel = false;
                errorProvider1.SetError(txtUsername, null);
            }
        }

        private void txtEmail_Validating(object sender, CancelEventArgs e)
        {
            if (!VerificationTexte.isValidMail(txtEmail.Text) || string.IsNullOrEmpty(txtEmail.Text))
            {
                e.Cancel = true;
                txtEmail.Focus();
                errorProvider1.SetError(txtEmail, "Veuillez entrer une adresse mail valide");
            }
            else
            {
                e.Cancel = false;
                errorProvider1.SetError(txtEmail, null);
            }
        }

        private void txtPhone_Validating(object sender, CancelEventArgs e)
        {
            if (!VerificationTexte.isValidPhoneNumber(txtPhone.Text) || string.IsNullOrEmpty(txtPhone.Text))
            {
                e.Cancel = true;
                txtPhone.Focus();
                errorProvider1.SetError(txtPhone, "Veuillez entrer un numéro de téléphone valide");
            }
            else
            {
                e.Cancel = false;
                errorProvider1.SetError(txtPhone, null);
            }
        }
    }
}
