﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LBC
{
    public class VerificationTexte
    {
        //Fonction pour mettre un mot en majuscule
        public static string ToUpperCase(string mot)
        {
            mot = mot.ToUpper();
            return mot;
        }

        //Fonction pour mettre un mot en minuscule
        public static string ToLowerCase(string mot)
        {
            mot = mot.ToLower();
            return mot;
        }

        //Fonction pour mettre la première lettre d'un mot en majuscule
        public static string FirstUpperCase(string mot)
        {
            if(mot.Length > 0)
            {
                mot = mot[0].ToString().ToUpper() + mot.Substring(1).ToLower();
                return mot;
            } else
            {
                return null;
            }
            
        }

        //Fonction pour vérifier un code postal valide
        public static bool isValidCodePostal(string mot)
        {
            return Regex.IsMatch(mot, @"^\d{5}$");
        }

        //Fonction pour vérifier un numéro de téléphone est valide
        public static bool isValidPhoneNumber(string mot)
        {
            return Regex.IsMatch(mot, @"^\d{10}$");
        }

        //Fonction pour vérifier si un nom ou un prénom est valide
        public static bool isValidNom(string mot)
        {
            return Regex.IsMatch(mot, @"^[^ ][a-zA-Z '\-éèêëçäàîï]*$");
        }

        //Fonction pour vérifier si l'adresse e mail est valide
        public static bool isValidMail(string mail_address)
        {
            Regex myRegex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", RegexOptions.IgnoreCase);
            return myRegex.IsMatch(mail_address);
        }

        public static bool isValidUsername(string username)
        {
            Regex myRegex = new Regex(@"^[0-9A-Za-z]*$");
            return myRegex.IsMatch(username);
        }

    }
}
