﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LBC
{
    public partial class UCVentirad : UserControl
    {
        public UCVentirad()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.VE0001;
            pictureBox2.Image = Properties.Resources.VE0002;
            pictureBox3.Image = Properties.Resources.VE0003;
            pictureBox4.Image = Properties.Resources.VE0004;
            pictureBox5.Image = Properties.Resources.VE0005;
            pictureBox7.Image = Properties.Resources.VE0009;
            pictureBox8.Image = Properties.Resources.VE0008;
            pictureBox9.Image = Properties.Resources.VE0007;
            pictureBox10.Image = Properties.Resources.VE0006;
        }

        private void UCVentirad_Load(object sender, EventArgs e)
        {
            //Chargement des produits enregistrés dans la base de données

                //Connexion à la base de données
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                //commande
                SqlCommand cmd = new SqlCommand("SELECT produit,prix,marque FROM ventirad", thisConnection);

                //Déclaration des listes
                List<string> productName = new List<string>();
                List<double> price = new List<double>();

                SqlDataReader DataRead;
                DataRead = cmd.ExecuteReader();
                //Récupération des données utilisateur
                while (DataRead.Read())
                {
                    productName.Add(DataRead["marque"].ToString() + " " + DataRead["produit"].ToString());
                    price.Add(Double.Parse(DataRead["prix"].ToString()));
                }

                foreach (string name in productName)
                {
                    btnProduct1.Text = productName[0].ToString();
                    btnProduct2.Text = productName[1].ToString();
                    btnProduct3.Text = productName[2].ToString();
                    btnProduct4.Text = productName[3].ToString();
                    btnProduct5.Text = productName[4].ToString();
                    btnProduct6.Text = productName[5].ToString();
                    btnProduct7.Text = productName[6].ToString();
                    btnProduct8.Text = productName[7].ToString();
                    btnProduct9.Text = productName[8].ToString();
                }

                foreach (double euros in price)
                {
                    btnPrice1.Text = price[0].ToString() + " €";
                    btnPrice2.Text = price[1].ToString() + " €";
                    btnPrice3.Text = price[2].ToString() + " €";
                    btnPrice4.Text = price[3].ToString() + " €";
                    btnPrice5.Text = price[4].ToString() + " €";
                    btnPrice6.Text = price[5].ToString() + " €";
                    btnPrice7.Text = price[6].ToString() + " €";
                    btnPrice8.Text = price[7].ToString() + " €";
                    btnPrice9.Text = price[8].ToString() + " €";
                }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (qty1.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0001";
                myCommand.Parameters["@productName"].Value = btnProduct1.Text;
                myCommand.Parameters["@quantity"].Value = qty1.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice1.Text.Substring(0, btnPrice1.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (qty2.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0002";
                myCommand.Parameters["@productName"].Value = btnProduct2.Text;
                myCommand.Parameters["@quantity"].Value = qty2.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice2.Text.Substring(0, btnPrice2.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (qty3.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0003";
                myCommand.Parameters["@productName"].Value = btnProduct3.Text;
                myCommand.Parameters["@quantity"].Value = qty3.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice3.Text.Substring(0, btnPrice3.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (qty4.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0004";
                myCommand.Parameters["@productName"].Value = btnProduct4.Text;
                myCommand.Parameters["@quantity"].Value = qty4.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice4.Text.Substring(0, btnPrice4.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (qty5.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0005";
                myCommand.Parameters["@productName"].Value = btnProduct5.Text;
                myCommand.Parameters["@quantity"].Value = qty5.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice5.Text.Substring(0, btnPrice5.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (qty6.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0006";
                myCommand.Parameters["@productName"].Value = btnProduct6.Text;
                myCommand.Parameters["@quantity"].Value = qty6.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice6.Text.Substring(0, btnPrice6.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (qty7.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0007";
                myCommand.Parameters["@productName"].Value = btnProduct7.Text;
                myCommand.Parameters["@quantity"].Value = qty7.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice7.Text.Substring(0, btnPrice7.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (qty8.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0008";
                myCommand.Parameters["@productName"].Value = btnProduct8.Text;
                myCommand.Parameters["@quantity"].Value = qty8.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice8.Text.Substring(0, btnPrice8.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (qty9.Value != 0)
            {
                int NewElmt = 0;
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                SqlCommand myCommand = new SqlCommand("InsertToBasket", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                // Add Parameters to Command Parameters collection


                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@productName", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantity", SqlDbType.Int);
                myCommand.Parameters.Add("@productPrice", SqlDbType.Decimal);

                // Affectation des valeurs
                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@productRef"].Value = "VE0009";
                myCommand.Parameters["@productName"].Value = btnProduct9.Text;
                myCommand.Parameters["@quantity"].Value = qty9.Value;
                myCommand.Parameters["@productPrice"].Value = Double.Parse(btnPrice9.Text.Substring(0, btnPrice9.Text.IndexOf(" ")));
                // Affectation des valeurs

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt == 1)
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                    else
                    {
                        MessageBox.Show("Ajout au panier effectué!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
                thisConnection.Close();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas sélectionné de quantité");
            }
        }
    }
}
