﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LBC
{
    public partial class UCAccueil : UserControl
    {
        public UCAccueil()
        {
            InitializeComponent();
        }
        
        private void UCAccueil_Load(object sender, EventArgs e)
        {
            //remplissage champs profil
            txtFirstName.Text = CurrentUser.currentUser.UserFirstname;
            txtLastName.Text = CurrentUser.currentUser.UserLastname;
            txtUserName.Text = CurrentUser.currentUser.UserUsername;
            txtEmail.Text = CurrentUser.currentUser.UserEmail;
            txtAdress.Text = CurrentUser.currentUser.UserAdress;
            txtPhone.Text = CurrentUser.currentUser.UserPhone;
            txtPassword.Text = CurrentUser.currentUser.UserPassword;
        }
        
        //bouton de mise à jour des données
        private void btnChange_Click(object sender, EventArgs e) //btnUpdate
        {
            UpdateClient();
        }

        //appel procédure UpdateClient
        private void UpdateClient()
        {
            int NewElmt = 0;
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            SqlCommand myCommand = new SqlCommand("UpdateClient", thisConnection);
            myCommand.CommandType = CommandType.StoredProcedure;
            // Add Parameters to Command Parameters collection


            myCommand.Parameters.Add("@id", SqlDbType.Int);
            myCommand.Parameters.Add("@lastname", SqlDbType.VarChar, 50);
            myCommand.Parameters.Add("@firstname", SqlDbType.VarChar, 50);
            myCommand.Parameters.Add("@password", SqlDbType.VarChar, 50);
            myCommand.Parameters.Add("@email", SqlDbType.VarChar, 50);
            myCommand.Parameters.Add("@adress", SqlDbType.VarChar, 120);
            myCommand.Parameters.Add("@phone", SqlDbType.VarChar, 10);

            // Affectation des valeurs
            myCommand.Parameters["@id"].Value = CurrentUser.currentUser.UserId;
            myCommand.Parameters["@lastname"].Value = txtLastName.Text;
            myCommand.Parameters["@firstname"].Value = txtFirstName.Text;
            myCommand.Parameters["@password"].Value = txtPassword.Text;
            myCommand.Parameters["@email"].Value = txtEmail.Text;
            myCommand.Parameters["@adress"].Value = txtAdress.Text;
            myCommand.Parameters["@phone"].Value = txtPhone.Text;
            // Affectation des valeurs

            try
            {
                NewElmt = (Int32)myCommand.ExecuteScalar();

                if (NewElmt == 1)
                {
                    MessageBox.Show("Mise à jour OK");
                    CurrentUser.currentUser.UserLastname = txtLastName.Text;
                    CurrentUser.currentUser.UserFirstname = txtFirstName.Text;
                    CurrentUser.currentUser.UserPassword = txtPassword.Text;
                    CurrentUser.currentUser.UserEmail = txtEmail.Text;
                    CurrentUser.currentUser.UserPhone = txtPhone.Text;
                    CurrentUser.currentUser.UserAdress = txtAdress.Text;
                }
                else
                {
                    MessageBox.Show("Erreur!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "Erreur!");
                return;
            }
            thisConnection.Close();
        }

        private void lvOrders_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtPassword.UseSystemPasswordChar == true)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }

        private void btnAccessUpdate_Click(object sender, EventArgs e)
        {
            btnAccessUpdate.Visible = false;
            lblProfile.Visible = true;
            lblLastName.Visible = true;
            lblFirstName.Visible = true;
            lblPassword.Visible = true;
            lblEmail.Visible = true;
            lblAdress.Visible = true;
            lblPhone.Visible = true;
            txtLastName.Visible = true;
            txtFirstName.Visible = true;
            txtPassword.Visible = true;
            txtEmail.Visible = true;
            txtAdress.Visible = true;
            txtPhone.Visible = true;
            btnUpdate.Visible = true;
            button1.Visible = true;
            txtUserName.Visible = true;
            btnCancel.Visible = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnAccessUpdate.Visible = true;
            lblProfile.Visible = false;
            lblLastName.Visible = false;
            lblFirstName.Visible = false;
            lblPassword.Visible = false;
            lblEmail.Visible = false;
            lblAdress.Visible = false;
            lblPhone.Visible = false;
            txtLastName.Visible = false;
            txtFirstName.Visible = false;
            txtPassword.Visible = false;
            txtEmail.Visible = false;
            txtAdress.Visible = false;
            txtPhone.Visible = false;
            btnUpdate.Visible = false;
            button1.Visible = false;
            txtUserName.Visible = false;
            btnCancel.Visible = false;
        }
    }
}
