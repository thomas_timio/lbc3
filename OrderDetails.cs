﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace LBC
{
    public partial class OrderDetails : Form
    {
        string cmdnum;
        public OrderDetails(string numcomm)
        {
            InitializeComponent();
            cmdnum = numcomm;
        }

        private void OrderDetails_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(cmdnum.ToString());
            ListView listview = new ListView();
            lvOrder.View = View.Details;
            lvOrder.Clear();
            lvOrder.Columns.Add("Qte");
            lvOrder.Columns.Add("Ref");
            lvOrder.Columns.Add("Prix");

            
            SqlConnection thisconnect = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            SqlCommand cmd = new SqlCommand("FillOrderList", thisconnect);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@id", SqlDbType.VarChar,50);
            cmd.Parameters["@id"].Value = cmdnum.ToString();
            thisconnect.Open();
            SqlDataReader reader = null;
            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    // test de chargement du buffer MessageBox.Show($"{reader.GetValue(0).ToString()}  {reader.GetValue(1).ToString()}  {reader.GetValue(2).ToString()} {reader.GetValue(3).ToString()}");

                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = reader.GetValue(0).ToString();
                    item.SubItems.Add(reader.GetValue(1).ToString());
                    item.SubItems.Add(reader.GetValue(2).ToString());
                    lvOrder.Items.Add(item);
                    lblCmdNum.Text = reader.GetValue(3).ToString();
                    lblDelivAd.Text = reader.GetValue(4).ToString();
                    lblFactAdrCust.Text = reader.GetValue(5).ToString();
                    lblCmdDate.Text = reader.GetValue(6).ToString().Substring(0,10);
                    lblPrice.Text = reader.GetValue(7).ToString();
                    lblPaymentMethod.Text = reader.GetValue(8).ToString();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                thisconnect.Close();
            }

        }

        private void labLeave_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
