﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LBC
{
    public partial class App : Form
    {
        public App()
        {
            InitializeComponent();
        }

        //Chargement de l'app
        private void App_Load(object sender, EventArgs e)
        {
            txtFirstName.Text = CurrentUser.currentUser.UserFirstname;
            ucAccueil1.BringToFront();
        }


        

        private void userControl1_Load(object sender, EventArgs e)
        {
            
        }

        //bouton accueil
        private void button14_Click(object sender, EventArgs e)
        {
            ucAccueil1.BringToFront();
            littleSidePanel.Height = btnAccueil.Height;
            littleSidePanel.Top = btnAccueil.Top;
            littleSidePanel.Visible = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ucProc1.BringToFront();
            littleSidePanel.Height = button1.Height;
            littleSidePanel.Top = button1.Top;
            littleSidePanel.Visible = true;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            ucMotherboard1.BringToFront();
            littleSidePanel.Height = button2.Height;
            littleSidePanel.Top = button2.Top;
            littleSidePanel.Visible = true;
        }



        private void button3_Click(object sender, EventArgs e)
        {
            ucRam1.BringToFront();
            littleSidePanel.Height = button3.Height;
            littleSidePanel.Top = button3.Top;
            littleSidePanel.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ucGraph1.BringToFront();
            littleSidePanel.Height = button4.Height;
            littleSidePanel.Top = button4.Top;
            littleSidePanel.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ucPower1.BringToFront();
            littleSidePanel.Height = button6.Height;
            littleSidePanel.Top = button6.Top;
            littleSidePanel.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ucssd1.BringToFront();
            littleSidePanel.Height = button7.Height;
            littleSidePanel.Top = button7.Top;
            littleSidePanel.Visible = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            uchdd1.BringToFront();
            littleSidePanel.Height = button8.Height;
            littleSidePanel.Top = button8.Top;
            littleSidePanel.Visible = true;
        }


        private void button10_Click(object sender, EventArgs e)
        {
            ucBox1.BringToFront();
            littleSidePanel.Height = button10.Height;
            littleSidePanel.Top = button10.Top;
            littleSidePanel.Visible = true;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            ucScreen1.BringToFront();
            littleSidePanel.Height = button11.Height;
            littleSidePanel.Top = button11.Top;
            littleSidePanel.Visible = true;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ucKeyboard1.BringToFront();
            littleSidePanel.Height = button12.Height;
            littleSidePanel.Top = button12.Top;
            littleSidePanel.Visible = true;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            ucMouse1.BringToFront();
            littleSidePanel.Height = button13.Height;
            littleSidePanel.Top = button13.Top;
            littleSidePanel.Visible = true;
        }



        private void lblLeave_LinkClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblLeave_Click(object sender, EventArgs e)
        {
            var quitter = MessageBox.Show("Voulez vous quitter ?", "Quitter", MessageBoxButtons.YesNo);
            if(quitter == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void lblBasket_Click(object sender, EventArgs e)
        {
            ucBasket1.BringToFront();
            littleSidePanel.Visible = false;
        }

        private void lblMyOrders_Click(object sender, EventArgs e)
        {
            ucOrder1.BringToFront();
            littleSidePanel.Visible = false;
        }

        private void lblHome_Click(object sender, EventArgs e)
        {
            ucAccueil1.BringToFront();
            littleSidePanel.Height = btnAccueil.Height;
            littleSidePanel.Top = btnAccueil.Top;
            littleSidePanel.Visible = true;
        }
    }
}
