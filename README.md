# Application LBC

## Contexte
Cette application a été créée dans le cadre d'une formation AFPA préparatoire en .NET. Elle n'a pas vocation à être utilisée dans un but commercial.

## Création
L'application a été créée intégralement sur la technologie WinForms. Elle utilise une base de données SQL.

## Auteurs
Les auteurs de cette application sont en formation Développeur .NET. Il s'agit de :
- Thomas Desaegher
- Thomas Timio

## Fonctionnalités
1. Création / modification de compte
2. Ajout au panier
3. Procéder à l'achat
4. Afficher le détail des produits
5. Récupérer un mot de passe oublié

## Suites
Les auteurs continuent de valoriser leurs compétences, d'autres projets plus élaborés sont en cours de création sur différentes technologies.
