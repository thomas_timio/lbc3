﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LBC
{
    public partial class ProductDetail : Form
    {
        string prix;
        string nomProduit;
        string productRef;
        string SearchTable;
        public ProductDetail(string productR, string nomP)
        {
            nomProduit = nomP;
            productRef = productR;
            InitializeComponent();
        }

        private void ProductDetail_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(nomProduit);
            lblNomProduit.Text = nomProduit;
            string productRefShort = productRef.Substring(0, 2).ToString();
            pictureBox1.Image = (Image)Properties.Resources.ResourceManager.GetObject(productRef);
            switch (productRefShort)
            {
                case "CM":
                    SearchTable = "carte_mere";
                    break;

                case "AL":
                    SearchTable = "alim";
                    break;

                case "BO":
                    SearchTable = "boitier";
                    break;

                case "CG":
                    SearchTable = "carte_graphique";
                    break;

                case "KB":
                    SearchTable = "claviers";
                    break;

                case "SC":
                    SearchTable = "ecrans";
                    break;

                case "HD":
                    SearchTable = "HDD";
                    break;

                case "CP":
                    SearchTable = "processeurs";
                    break;

                case "RA":
                    SearchTable = "RAM";
                    break;

                case "MO":
                    SearchTable = "souris";
                    break;

                case "SD":
                    SearchTable = "SSD";
                    break;

                case "VE":
                    SearchTable = "ventirad";
                    break;
            }

            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            string sCommand = $"SELECT * FROM {SearchTable} WHERE Ref = @productRef";
            SqlCommand myCommand = new SqlCommand(sCommand, thisConnection);
            SqlDataReader reader = null;

            myCommand.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
            myCommand.Parameters["@productRef"].Value = productRef;

            try
            {
                reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //MessageBox.Show(reader.FieldCount.ToString());
                        switch (reader.FieldCount)
                        {
                            case 6:

                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Visible = false;
                                lbl8.Visible = false;
                                lbl9.Visible = false;
                                lbl10.Visible = false;
                                lbl11.Visible = false;
                                lbl12.Visible = false;
                                lbl13.Visible = false;
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                break;

                            case 7:
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Visible = false;
                                lbl9.Visible = false;
                                lbl10.Visible = false;
                                lbl11.Visible = false;
                                lbl12.Visible = false;
                                lbl13.Visible = false;
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                break;

                            case 8:
                                lbl9.Visible = false;
                                lbl10.Visible = false;
                                lbl11.Visible = false;
                                lbl12.Visible = false;
                                lbl13.Visible = false;
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                break;

                            case 9:
                                lbl10.Visible = false;
                                lbl11.Visible = false;
                                lbl12.Visible = false;
                                lbl13.Visible = false;
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                lbl9.Text = reader.GetName(8).ToString() + " : " + reader.GetValue(8).ToString();
                                break;

                            case 10:
                                lbl11.Visible = false;
                                lbl12.Visible = false;
                                lbl13.Visible = false;
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                lbl9.Text = reader.GetName(8).ToString() + " : " + reader.GetValue(8).ToString();
                                lbl10.Text = reader.GetName(9).ToString() + " : " + reader.GetValue(9).ToString();
                                break;

                            case 11:
                                lbl12.Visible = false;
                                lbl13.Visible = false;
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                lbl9.Text = reader.GetName(8).ToString() + " : " + reader.GetValue(8).ToString();
                                lbl10.Text = reader.GetName(9).ToString() + " : " + reader.GetValue(9).ToString();
                                lbl11.Text = reader.GetName(10).ToString() + " : " + reader.GetValue(10).ToString();
                                break;

                            case 12:
                                lbl13.Visible = false;
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                lbl9.Text = reader.GetName(8).ToString() + " : " + reader.GetValue(8).ToString();
                                lbl10.Text = reader.GetName(9).ToString() + " : " + reader.GetValue(9).ToString();
                                lbl11.Text = reader.GetName(10).ToString() + " : " + reader.GetValue(10).ToString();
                                lbl12.Text = reader.GetName(11).ToString() + " : " + reader.GetValue(11).ToString();

                                break;

                            case 13:
                                lbl14.Visible = false;
                                lbl15.Visible = false;
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                lbl9.Text = reader.GetName(8).ToString() + " : " + reader.GetValue(8).ToString();
                                lbl10.Text = reader.GetName(9).ToString() + " : " + reader.GetValue(9).ToString();
                                lbl11.Text = reader.GetName(10).ToString() + " : " + reader.GetValue(10).ToString();
                                lbl12.Text = reader.GetName(11).ToString() + " : " + reader.GetValue(11).ToString();
                                lbl13.Text = reader.GetName(12).ToString() + " : " + reader.GetValue(12).ToString();
                                break;

                            case 14:
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                lbl9.Text = reader.GetName(8).ToString() + " : " + reader.GetValue(8).ToString();
                                lbl10.Text = reader.GetName(9).ToString() + " : " + reader.GetValue(9).ToString();
                                lbl11.Text = reader.GetName(10).ToString() + " : " + reader.GetValue(10).ToString();
                                lbl12.Text = reader.GetName(11).ToString() + " : " + reader.GetValue(11).ToString();
                                lbl13.Text = reader.GetName(12).ToString() + " : " + reader.GetValue(12).ToString();
                                lbl14.Text = reader.GetName(13).ToString() + " : " + reader.GetValue(13).ToString();
                                lbl15.Visible = false;
                                break;

                            case 15:
                                lbl1.Text = reader.GetName(0).ToString() + " : " + reader.GetValue(0).ToString();
                                lbl2.Text = reader.GetName(1).ToString() + " : " + reader.GetValue(1).ToString();
                                lbl3.Text = reader.GetName(2).ToString() + " : " + reader.GetValue(2).ToString();
                                lbl4.Text = reader.GetName(3).ToString() + " : " + reader.GetValue(3).ToString();
                                lbl5.Text = reader.GetName(4).ToString() + " : " + reader.GetValue(4).ToString();
                                lbl6.Text = reader.GetName(5).ToString() + " : " + reader.GetValue(5).ToString();
                                lbl7.Text = reader.GetName(6).ToString() + " : " + reader.GetValue(6).ToString();
                                lbl8.Text = reader.GetName(7).ToString() + " : " + reader.GetValue(7).ToString();
                                lbl9.Text = reader.GetName(8).ToString() + " : " + reader.GetValue(8).ToString();
                                lbl10.Text = reader.GetName(9).ToString() + " : " + reader.GetValue(9).ToString();
                                lbl11.Text = reader.GetName(10).ToString() + " : " + reader.GetValue(10).ToString();
                                lbl12.Text = reader.GetName(11).ToString() + " : " + reader.GetValue(11).ToString();
                                lbl13.Text = reader.GetName(12).ToString() + " : " + reader.GetValue(12).ToString();
                                lbl14.Text = reader.GetName(13).ToString() + " : " + reader.GetValue(13).ToString();
                                lbl15.Text = reader.GetName(13).ToString() + " : " + reader.GetValue(14).ToString();
                                break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
            finally
            {
                reader.Close();
                thisConnection.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBasket_Click(object sender, EventArgs e)
        {
            int NewElmt = 0;
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            SqlCommand Command = new SqlCommand("InsertToBasket", thisConnection);
            SqlCommand cmd2 = new SqlCommand($"SELECT prix FROM {SearchTable} WHERE Ref = @Ref", thisConnection);
            Command.CommandType = CommandType.StoredProcedure;
            // Add Parameters to Command Parameters collection

            cmd2.Parameters.Add("@Ref", SqlDbType.VarChar, 50);
            Command.Parameters.Add("@currentUser_id", SqlDbType.Int);
            Command.Parameters.Add("@productRef", SqlDbType.VarChar, 50);
            Command.Parameters.Add("@productName", SqlDbType.VarChar, 50);
            Command.Parameters.Add("@quantity", SqlDbType.Int);
            Command.Parameters.Add("@productPrice", SqlDbType.Decimal);

            //Affectation des valeurs
            cmd2.Parameters["@Ref"].Value = productRef;
            Command.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
            Command.Parameters["@productRef"].Value = productRef;
            Command.Parameters["@productName"].Value = lblNomProduit.Text;
            Command.Parameters["@quantity"].Value = 1;

            SqlDataReader reader2 = null;

            try
            {
                reader2 = cmd2.ExecuteReader();
                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        prix = reader2.GetValue(0).ToString();
                    }
                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.ToString());
                return;
            }
            finally
            {
                reader2.Close();
            }
        
            Command.Parameters["@productPrice"].Value = Double.Parse(prix);

            try
            {
                NewElmt = (Int32)Command.ExecuteScalar();

                if (NewElmt == 1)
                {
                    MessageBox.Show("Ajout au panier effectué!");
                }
                else
                {
                    MessageBox.Show("Ajout au panier effectué!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "Erreur!");
                return;
            }
            thisConnection.Close();
        }

        private void lblLeave_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
