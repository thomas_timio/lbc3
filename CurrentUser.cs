﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LBC
{
    class CurrentUser
    {
        int id;
        string lastname;
        string firstname;
        string username;
        string password;
        string email;
        string adress;
        string phone;

        public static CurrentUser currentUser = new CurrentUser(0, "", "", "", "", "", "", "");

        public CurrentUser()
        {

        }

        public CurrentUser(int Id, string LastName, string FirstName, string Username, string Password, string Email, string Adress, string Phone)
        {
            id = Id;
            lastname = LastName;
            firstname = FirstName;
            username = Username;
            password = Username;
            email = Email;
            adress = Adress;
            phone = Phone;
        }

        public int UserId
        {
            get { return id; }
            set { id = value; }
        }

        public string UserLastname
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string UserFirstname
        {
            get { return firstname; }
            set { firstname = value; }
        }
        public string UserUsername
        {
            get { return username; }
            set { username = value; }
        }
        public string UserPassword
        {
            get { return password; }
            set { password = value; }
        }
        public string UserEmail
        {
            get { return email; }
            set { email = value; }
        }
        public string UserPhone
        {
            get { return phone; }
            set { phone = value; }
        }
        public string UserAdress
        {
            get { return adress; }
            set { adress = value; }
        }
    }
}