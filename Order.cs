﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LBC
{
    public partial class Order : Form
    {
        string order_number = CurrentUser.currentUser.UserId.ToString() + "-" + FormatDate(DateTime.Now.ToString());
        DateTime date = DateTime.Now;

        public Order()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            var resulat = MessageBox.Show("Procéder au paiement?", "Confirmation", MessageBoxButtons.YesNo);
            if(resulat == DialogResult.Yes)
            {
                InsertAllProducts(GetAllProductRef(), GetAllQuantite(), GetAllPrices());
                InsertOrderInfo();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Order_Load(object sender, EventArgs e)
        {
            txtLivraison.Text = CurrentUser.currentUser.UserAdress;
            txtFacturation.Text = CurrentUser.currentUser.UserAdress;
            btnPrice.Text = GetTotalPrice().ToString() + " €";
        }

        public void OrderFunction(List<string> reference, List<int> quantity)
        {
            
        }

        public double GetTotalPrice()
        {
            double total_price = 0;
            //Connexion à la base de données
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            //Requête à effectuer
            SqlCommand cmd = new SqlCommand("SELECT prix_produit FROM panier WHERE id_utilisateur = @currentuser_id", thisConnection);
            cmd.Parameters.Add("@currentUser_id", SqlDbType.Int);
            cmd.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;

            List<double> priceList = new List<double>();

            //Récupération des données utilisateur
            SqlDataReader DataRead;
            DataRead = cmd.ExecuteReader();
            while (DataRead.Read())
            {
                priceList.Add(Double.Parse(DataRead["prix_produit"].ToString()));
            }
            thisConnection.Close();
            
            foreach(double price in priceList)
            {
                total_price += price;
            }

            return total_price;

        }

        public List<string> GetAllProductRef()
        {
            //Connexion à la base de données
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            //Requête à effectuer
            SqlCommand cmd = new SqlCommand("SELECT ref_produit FROM panier WHERE id_utilisateur = @currentuser_id", thisConnection);
            cmd.Parameters.Add("@currentUser_id", SqlDbType.Int);
            cmd.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;

            List<string> productRef = new List<string>();

            //Récupération des données utilisateur
            SqlDataReader DataRead;
            DataRead = cmd.ExecuteReader();
            while (DataRead.Read())
            {
                productRef.Add(DataRead["ref_produit"].ToString());
            }
            thisConnection.Close();
            return productRef;
            
        }

        public List<int> GetAllQuantite()
        {
            //Connexion à la base de données
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            //Requête à effectuer
            SqlCommand cmd = new SqlCommand("SELECT quantite FROM panier WHERE id_utilisateur = @currentuser_id", thisConnection);
            cmd.Parameters.Add("@currentUser_id", SqlDbType.Int);
            cmd.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;

            List<int> quantite = new List<int>();

            //Récupération des données utilisateur
            SqlDataReader DataRead;
            DataRead = cmd.ExecuteReader();
            while (DataRead.Read())
            {
                quantite.Add((int)DataRead["quantite"]);
            }
            thisConnection.Close();
            return quantite;
        }
        public List<double> GetAllPrices()
        {
            //Connexion à la base de données
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            //Requête à effectuer
            SqlCommand cmd = new SqlCommand("SELECT prix_produit FROM panier WHERE id_utilisateur = @currentuser_id", thisConnection);
            cmd.Parameters.Add("@currentUser_id", SqlDbType.Int);
            cmd.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;

            List<double> priceList = new List<double>();

            //Récupération des données utilisateur
            SqlDataReader DataRead;
            DataRead = cmd.ExecuteReader();
            while (DataRead.Read())
            {
                priceList.Add(Double.Parse(DataRead["prix_produit"].ToString()));
            }
            thisConnection.Close();
            return priceList;
        }

        public void InsertAllProducts(List<string> reference, List<int> quantity, List<double> priceList)
        {
            int NewElmt = 0;

            for (int i = 0; i < reference.Count; i++)
            {
                //Connexion à la base de données
                SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
                thisConnection.Open();
                //Requête à effectuer
                SqlCommand myCommand = new SqlCommand("InsertAllProducts", thisConnection);
                myCommand.CommandType = CommandType.StoredProcedure;

                myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
                myCommand.Parameters.Add("@numero_commande", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@quantite", SqlDbType.Int);
                myCommand.Parameters.Add("@ref_produit", SqlDbType.VarChar, 50);
                myCommand.Parameters.Add("@prix_produit", SqlDbType.Decimal);

                myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
                myCommand.Parameters["@numero_commande"].Value = order_number.ToString();
                myCommand.Parameters["@ref_produit"].Value = reference[i].ToString();
                myCommand.Parameters["@quantite"].Value = (int)quantity[i];
                myCommand.Parameters["@prix_produit"].Value = Double.Parse(priceList[i].ToString());

                try
                {
                    NewElmt = (Int32)myCommand.ExecuteScalar();

                    if (NewElmt != 1)
                    {
                        MessageBox.Show("Erreur lors de l'achat!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + "Erreur!");
                    return;
                }
            }
            
        }

        public void InsertOrderInfo()
        {
            int NewElmt = 0;

           

            string delivery_adress = txtLivraison.Text;
            string bill_adress = txtFacturation.Text;
            string payment_method = "";
            if (rb1.Checked == true)
            {
                payment_method = "Visa";
            }
            if (rb2.Checked == true)
            {
                payment_method = "Mastercard";
            }
            if (rb3.Checked == true)
            {
                payment_method = "Paypal";
            }
            
            //Connexion à la base de données
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();

            SqlCommand myCommand = new SqlCommand("InsertOrderInfo", thisConnection);
            myCommand.CommandType = CommandType.StoredProcedure;
            
            // Variables
            myCommand.Parameters.Add("@currentUser_id", SqlDbType.Int);
            myCommand.Parameters.Add("@numero_commande", SqlDbType.VarChar, 50);
            myCommand.Parameters.Add("@adresse_livraison", SqlDbType.VarChar, 150);
            myCommand.Parameters.Add("@adresse_facturation", SqlDbType.VarChar, 150);
            myCommand.Parameters.Add("@date", SqlDbType.DateTime);
            myCommand.Parameters.Add("@prix_total", SqlDbType.Decimal);
            myCommand.Parameters.Add("@moyen_paiement", SqlDbType.VarChar, 50);

            myCommand.Parameters["@currentUser_id"].Value = CurrentUser.currentUser.UserId;
            myCommand.Parameters["@numero_commande"].Value = order_number.ToString();
            myCommand.Parameters["@date"].Value = date.ToString();
            myCommand.Parameters["@adresse_livraison"].Value = delivery_adress;
            myCommand.Parameters["@adresse_facturation"].Value = bill_adress;
            myCommand.Parameters["@prix_total"].Value = GetTotalPrice();
            myCommand.Parameters["@moyen_paiement"].Value = payment_method;
            
            try
            {
                NewElmt = (Int32)myCommand.ExecuteScalar();

                if (NewElmt == 1)
                {
                    MessageBox.Show("Achat effectué!");
                    this.Close();
                    OrderDetails orderDetails = new OrderDetails(order_number.ToString());
                    orderDetails.ShowDialog();
                    SendMail();
                }
                else
                {
                    MessageBox.Show("Erreur lors de l'achat!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "Erreur!");
                return;
            }

            thisConnection.Close();
        }

        public static string FormatDate(string mot)
        {
            string mot1;
            string mot2;
            string mot3;
            mot1 = mot.Replace("/", "");
            mot2 = mot1.Replace(":", "");
            mot3 = mot2.Replace(" ", "");
            return mot3;
            
        }

        private void lblLeave_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SendMail()
        {
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            //code pour envoyer un mail 
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress("lbcafpa@outlook.fr");
            message.To.Add(new MailAddress(CurrentUser.currentUser.UserEmail));
            message.Subject = "Confirmation de commande";
            message.Body = $"Votre commande n°{order_number} a bien été validée! ";
            smtp.Port = 587;
            smtp.Host = "smtp.office365.com";
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("lbcafpa@outlook.fr", "ProutProut");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur " + ex.ToString());
            }
            finally
            {
                thisConnection.Close();
            }
        }

        private void checkBoxAdress_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxAdress.Checked == true)
            {
                txtFacturation.Text = txtLivraison.Text;
            } else
            {
                txtFacturation.Text = "";
            }
        }
    }
}
