﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LBC
{
    public partial class UCBasket : UserControl
    {
        Order order = new Order();
        int valeur;

        public UCBasket()
        {
            InitializeComponent();
        }

        private void lvBasket_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void UCBasket_Load(object sender, EventArgs e)
        {
            if (lvBasket.FocusedItem == null)
            {
                btnDel.Visible = false;
                btnMoins.Visible = false;
                btnPlus.Visible = false;
            }
        }

        private void UCBasket_Click(object sender, EventArgs e)
        {

        }

        private void UCBasket_Enter(object sender, EventArgs e)
        {

            ListView listview = new ListView();
            lvBasket.View = View.Details;
            lvBasket.Clear();
            lvBasket.Columns.Add("Qte");
            lvBasket.Columns.Add("Ref");
            lvBasket.Columns.Add("nom");
            lvBasket.Columns[2].Width = 520;
            lvBasket.Columns.Add("montant");
            lvBasket.Columns[3].TextAlign = HorizontalAlignment.Right;

            SqlConnection thisconnect = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            SqlCommand cmd = new SqlCommand("SELECT quantite, ref_produit, nom_produit, prix_produit FROM panier WHERE id_utilisateur = @userId ", thisconnect);
            cmd.Parameters.Add("@userId", SqlDbType.Int);
            cmd.Parameters["@userId"].Value = CurrentUser.currentUser.UserId;
            thisconnect.Open();
            SqlDataReader reader = null;
            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                   // test de chargement du buffer MessageBox.Show($"{reader.GetValue(0).ToString()}  {reader.GetValue(1).ToString()}  {reader.GetValue(2).ToString()} {reader.GetValue(3).ToString()}");

                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = reader.GetValue(0).ToString();
                    item.SubItems.Add(reader.GetValue(1).ToString());
                    item.SubItems.Add(reader.GetValue(2).ToString());
                    item.SubItems.Add(reader.GetValue(3).ToString());
                    lvBasket.Items.Add(item);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                thisconnect.Close();
            }
        }

        private void btnRafraichir_Click(object sender, EventArgs e)
        {
            ListView listview = new ListView();
            lvBasket.View = View.Details;
            lvBasket.Clear();
            lvBasket.Columns.Add("Qte");
            lvBasket.Columns.Add("Ref");
            lvBasket.Columns.Add("nom");
            lvBasket.Columns[2].Width = 520;
            lvBasket.Columns.Add("montant");
            lvBasket.Columns[3].TextAlign = HorizontalAlignment.Right;

            SqlConnection thisconnect = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            SqlCommand cmd = new SqlCommand("SELECT quantite, ref_produit, nom_produit, prix_produit FROM panier WHERE id_utilisateur = @userId ", thisconnect);
            cmd.Parameters.Add("@userId", SqlDbType.Int);
            cmd.Parameters["@userId"].Value = CurrentUser.currentUser.UserId;
            thisconnect.Open();
            SqlDataReader reader = null;
            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    // test de chargement du buffer MessageBox.Show($"{reader.GetValue(0).ToString()}  {reader.GetValue(1).ToString()}  {reader.GetValue(2).ToString()} {reader.GetValue(3).ToString()}");

                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = reader.GetValue(0).ToString();
                    item.SubItems.Add(reader.GetValue(1).ToString());
                    item.SubItems.Add(reader.GetValue(2).ToString());
                    item.SubItems.Add(reader.GetValue(3).ToString());
                    lvBasket.Items.Add(item);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                thisconnect.Close();
                //MessageBox.Show(order.GetTotalPrice().ToString());
                btnTotalPrice.Text = "Total : " + order.GetTotalPrice().ToString() + " €";
            }
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
            Order order = new Order();
            order.Show();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            DeleteFromBasket();
        }

        private void lvBasket_Click(object sender, EventArgs e)
        {
            
        }

        private void lvBasket_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lvBasket.SelectedItems.Count > 0)
            {
                btnDel.Visible = true;
                btnPlus.Visible = true;
                btnMoins.Visible = true;
            }
            else
            {
                btnDel.Visible = false;
                btnMoins.Visible = false;
                btnPlus.Visible = false;
            }
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            SqlConnection thisconnect = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            SqlCommand cmd = new SqlCommand("UPDATE panier SET prix_produit += prix_produit / quantite , quantite = quantite+1 WHERE id_utilisateur = @id AND ref_produit = @ref", thisconnect);
            cmd.Parameters.Add("@id", SqlDbType.Int);
            cmd.Parameters["@id"].Value = CurrentUser.currentUser.UserId;
            cmd.Parameters.Add("@ref", SqlDbType.VarChar, 50);
            cmd.Parameters["@ref"].Value = lvBasket.FocusedItem.SubItems[1].Text;
            thisconnect.Open();

            try
            {
                cmd.ExecuteNonQuery();
                lvBasket.View = View.Details;
                lvBasket.Clear();
                lvBasket.Columns.Add("Qte");
                lvBasket.Columns.Add("Ref");
                lvBasket.Columns.Add("nom");
                lvBasket.Columns[2].Width = 520;
                lvBasket.Columns.Add("montant");
                lvBasket.Columns[3].TextAlign = HorizontalAlignment.Right;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            SqlCommand mycmd = new SqlCommand("SELECT quantite, ref_produit, nom_produit, prix_produit FROM panier WHERE id_utilisateur = @userId ", thisconnect);
            mycmd.Parameters.Add("@userId", SqlDbType.Int);
            mycmd.Parameters["@userId"].Value = CurrentUser.currentUser.UserId;

            SqlDataReader reader = null;
            try
            {
                reader = mycmd.ExecuteReader();
                while (reader.Read())
                {
                    // test de chargement du buffer MessageBox.Show($"{reader.GetValue(0).ToString()}  {reader.GetValue(1).ToString()}  {reader.GetValue(2).ToString()} {reader.GetValue(3).ToString()}");

                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = reader.GetValue(0).ToString();
                    item.SubItems.Add(reader.GetValue(1).ToString());
                    item.SubItems.Add(reader.GetValue(2).ToString());
                    item.SubItems.Add(reader.GetValue(3).ToString());
                    lvBasket.Items.Add(item);

                }

            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.ToString());
            }
            finally
            {
                thisconnect.Close();
                btnDel.Visible = false;
                btnPlus.Visible = false;
                btnMoins.Visible = false;
                btnTotalPrice.Text = "Total : " + order.GetTotalPrice().ToString() + " €";
            }
        }

        private void btnMoins_Click(object sender, EventArgs e)
        {
            SqlConnection thisconnect = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            SqlCommand cmd = new SqlCommand("UPDATE panier SET prix_produit -= prix_produit / quantite , quantite = quantite-1 WHERE id_utilisateur = @id AND ref_produit = @ref DELETE FROM panier WHERE quantite = 0", thisconnect);
            cmd.Parameters.Add("@id", SqlDbType.Int);
            cmd.Parameters["@id"].Value = CurrentUser.currentUser.UserId;
            cmd.Parameters.Add("@ref", SqlDbType.VarChar, 50);
            cmd.Parameters["@ref"].Value = lvBasket.FocusedItem.SubItems[1].Text;
            thisconnect.Open();

            try
            {
                cmd.ExecuteNonQuery();
                lvBasket.View = View.Details;
                lvBasket.Clear();
                lvBasket.Columns.Add("Qte");
                lvBasket.Columns.Add("Ref");
                lvBasket.Columns.Add("nom");
                lvBasket.Columns[2].Width = 520;
                lvBasket.Columns.Add("montant");
                lvBasket.Columns[3].TextAlign = HorizontalAlignment.Right;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            SqlCommand mycmd = new SqlCommand("SELECT quantite, ref_produit, nom_produit, prix_produit FROM panier WHERE id_utilisateur = @userId ", thisconnect);
            mycmd.Parameters.Add("@userId", SqlDbType.Int);
            mycmd.Parameters["@userId"].Value = CurrentUser.currentUser.UserId;

            SqlDataReader reader = null;
            try
            {
                reader = mycmd.ExecuteReader();
                while (reader.Read())
                {
                    // test de chargement du buffer MessageBox.Show($"{reader.GetValue(0).ToString()}  {reader.GetValue(1).ToString()}  {reader.GetValue(2).ToString()} {reader.GetValue(3).ToString()}");

                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = reader.GetValue(0).ToString();
                    item.SubItems.Add(reader.GetValue(1).ToString());
                    item.SubItems.Add(reader.GetValue(2).ToString());
                    item.SubItems.Add(reader.GetValue(3).ToString());
                    lvBasket.Items.Add(item);
                }

            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.ToString());
            }
            finally
            {
                thisconnect.Close();
                btnDel.Visible = false;
                btnPlus.Visible = false;
                btnMoins.Visible = false;
                btnTotalPrice.Text = "Total : " + order.GetTotalPrice().ToString() + " €";
            }

        }

        public void DeleteFromBasket()
        {
            SqlConnection thisconnect = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            SqlCommand cmd = new SqlCommand("DeleteFromBasket", thisconnect);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ref", SqlDbType.VarChar, 50);
            cmd.Parameters["@ref"].Value = lvBasket.FocusedItem.SubItems[1].Text;
            thisconnect.Open();

            try
            {
                cmd.ExecuteNonQuery();
                lvBasket.View = View.Details;
                lvBasket.Clear();
                lvBasket.Columns.Add("Qte");
                lvBasket.Columns.Add("Ref");
                lvBasket.Columns.Add("nom");
                lvBasket.Columns[2].Width = 520;
                lvBasket.Columns.Add("montant");
                lvBasket.Columns[3].TextAlign = HorizontalAlignment.Right;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            SqlCommand mycmd = new SqlCommand("SELECT quantite, ref_produit, nom_produit, prix_produit FROM panier WHERE id_utilisateur = @userId ", thisconnect);
            mycmd.Parameters.Add("@userId", SqlDbType.Int);
            mycmd.Parameters["@userId"].Value = CurrentUser.currentUser.UserId;

            SqlDataReader reader = null;
            try
            {
                reader = mycmd.ExecuteReader();
                while (reader.Read())
                {
                    // test de chargement du buffer MessageBox.Show($"{reader.GetValue(0).ToString()}  {reader.GetValue(1).ToString()}  {reader.GetValue(2).ToString()} {reader.GetValue(3).ToString()}");

                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = reader.GetValue(0).ToString();
                    item.SubItems.Add(reader.GetValue(1).ToString());
                    item.SubItems.Add(reader.GetValue(2).ToString());
                    item.SubItems.Add(reader.GetValue(3).ToString());
                    lvBasket.Items.Add(item);
                }

            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.ToString());
            }
            finally
            {
                thisconnect.Close();
                btnTotalPrice.Text = "Total : " + order.GetTotalPrice().ToString() + " €";
            }
        }
    }
}
