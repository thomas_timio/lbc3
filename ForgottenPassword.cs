﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Data.SqlClient;

namespace LBC
{
    public partial class ForgottenPassword : Form
    {
        public ForgottenPassword()
        {
            InitializeComponent();
        }
        
        
        private void Send_Click(object sender, EventArgs e)
        {
            
            SqlConnection thisConnection = new SqlConnection("Data Source =.\\SQLEXPRESS; database = LBC; integrated security = SSPI");
            thisConnection.Open();
            SqlCommand myCommand = new SqlCommand("ForgottenPassword", thisConnection);
            myCommand.CommandType = CommandType.StoredProcedure;
            // Add Parameters to Command Parameters collection

            myCommand.Parameters.Add("@username", SqlDbType.VarChar, 50);

            // Affectation des valeurs

            myCommand.Parameters["@username"].Value = txtUsername.Text;
            SqlDataReader reader = null;

            try
            {

                reader = myCommand.ExecuteReader();  //buffer qui contiendra le résultat de la requête
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {



                        //code pour envoyer un mail 
                        MailMessage message = new MailMessage();
                        SmtpClient smtp = new SmtpClient();
                        message.From = new MailAddress("lbcafpa@outlook.fr");
                        message.To.Add(new MailAddress(reader.GetValue(1).ToString()));
                        message.Subject = "Votre mot de passe";
                        message.Body = $"Voici votre Login : {reader.GetValue(2).ToString()} \nVoici le mot de passe pour votre compte : {reader.GetValue(0).ToString()}";
                        smtp.Port = 587;
                        smtp.Host = "smtp.office365.com";
                        smtp.EnableSsl = true;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential("lbcafpa@outlook.fr", "ProutProut");
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        try
                        {
                            var answerSend = MessageBox.Show($"Le mot de passe sera envoyé à l'adresse suivante: {reader.GetValue(1).ToString()}", "Confirmation", MessageBoxButtons.YesNo);
                            if (answerSend == DialogResult.Yes)
                            {
                                smtp.Send(message);
                                MessageBox.Show("Votre mot de passe vous a été envoyé dans votre boîte mail");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Erreur " + ex.ToString());
                        }
                        finally
                        {
                            this.Close();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Ce compte n'existe pas!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "Erreur!");
                return;
            }
            finally
            {
                thisConnection.Close();
            }


        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }
    }
}
