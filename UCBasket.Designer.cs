﻿namespace LBC
{
    partial class UCBasket
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvBasket = new System.Windows.Forms.ListView();
            this.btnOrder = new System.Windows.Forms.Button();
            this.btnRafraichir = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnMoins = new System.Windows.Forms.Button();
            this.btnPlus = new System.Windows.Forms.Button();
            this.btnTotalPrice = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvBasket
            // 
            this.lvBasket.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvBasket.FullRowSelect = true;
            this.lvBasket.HideSelection = false;
            this.lvBasket.Location = new System.Drawing.Point(73, 116);
            this.lvBasket.Name = "lvBasket";
            this.lvBasket.Size = new System.Drawing.Size(933, 332);
            this.lvBasket.TabIndex = 0;
            this.lvBasket.UseCompatibleStateImageBehavior = false;
            this.lvBasket.View = System.Windows.Forms.View.List;
            this.lvBasket.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvBasket_ItemSelectionChanged);
            this.lvBasket.SelectedIndexChanged += new System.EventHandler(this.lvBasket_SelectedIndexChanged);
            this.lvBasket.Click += new System.EventHandler(this.lvBasket_Click);
            // 
            // btnOrder
            // 
            this.btnOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.792F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnOrder.Location = new System.Drawing.Point(857, 447);
            this.btnOrder.Name = "btnOrder";
            this.btnOrder.Size = new System.Drawing.Size(150, 50);
            this.btnOrder.TabIndex = 1;
            this.btnOrder.Text = "Commander";
            this.btnOrder.UseVisualStyleBackColor = true;
            this.btnOrder.Click += new System.EventHandler(this.btnOrder_Click);
            // 
            // btnRafraichir
            // 
            this.btnRafraichir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRafraichir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.792F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRafraichir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(29)))), ((int)(((byte)(0)))));
            this.btnRafraichir.Location = new System.Drawing.Point(73, 447);
            this.btnRafraichir.Name = "btnRafraichir";
            this.btnRafraichir.Size = new System.Drawing.Size(150, 50);
            this.btnRafraichir.TabIndex = 2;
            this.btnRafraichir.Text = "Rafraichir";
            this.btnRafraichir.UseVisualStyleBackColor = true;
            this.btnRafraichir.Click += new System.EventHandler(this.btnRafraichir_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(29)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(73, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(933, 43);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mon panier";
            // 
            // btnDel
            // 
            this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.792F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.Location = new System.Drawing.Point(335, 447);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(387, 50);
            this.btnDel.TabIndex = 4;
            this.btnDel.Text = "Supprimer la ligne";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnMoins
            // 
            this.btnMoins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoins.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.792F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoins.Location = new System.Drawing.Point(335, 496);
            this.btnMoins.Name = "btnMoins";
            this.btnMoins.Size = new System.Drawing.Size(196, 50);
            this.btnMoins.TabIndex = 5;
            this.btnMoins.Text = "-";
            this.btnMoins.UseVisualStyleBackColor = true;
            this.btnMoins.Click += new System.EventHandler(this.btnMoins_Click);
            // 
            // btnPlus
            // 
            this.btnPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.792F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlus.Location = new System.Drawing.Point(531, 496);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(191, 50);
            this.btnPlus.TabIndex = 6;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            this.btnPlus.Click += new System.EventHandler(this.btnPlus_Click);
            // 
            // btnTotalPrice
            // 
            this.btnTotalPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.792F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalPrice.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnTotalPrice.Location = new System.Drawing.Point(382, 563);
            this.btnTotalPrice.Name = "btnTotalPrice";
            this.btnTotalPrice.Size = new System.Drawing.Size(298, 50);
            this.btnTotalPrice.TabIndex = 7;
            this.btnTotalPrice.Text = "Total : ----- €";
            this.btnTotalPrice.UseVisualStyleBackColor = true;
            // 
            // UCBasket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnTotalPrice);
            this.Controls.Add(this.btnPlus);
            this.Controls.Add(this.btnMoins);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnRafraichir);
            this.Controls.Add(this.btnOrder);
            this.Controls.Add(this.lvBasket);
            this.Name = "UCBasket";
            this.Size = new System.Drawing.Size(1100, 780);
            this.Load += new System.EventHandler(this.UCBasket_Load);
            this.Click += new System.EventHandler(this.UCBasket_Click);
            this.Enter += new System.EventHandler(this.UCBasket_Enter);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvBasket;
        private System.Windows.Forms.Button btnOrder;
        private System.Windows.Forms.Button btnRafraichir;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnMoins;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Button btnTotalPrice;
    }
}
